# Darkula for [Obsidian.md](https://obsidian.md)

> A dark theme for [Obsidian](https://obisidian.md), compatible to Obsidian V0.9.1+

![Screenshot](./darkula.png)

![Screenshot](./darkula-2.png)

## Darkula theme is based on official Dracula theme but
- Left Menu is *always open*, life is too short for hovering menus :p
- You see your scrolls bar
- Consistent re-styling (on icon, buttons, etc.) v1.0
- You can prevent some content to be rendered using the *hideMe* class
    - exemple : ```<div class="hideMe">this content is on only visible in edit mode</div>```

## Install
1. Download the obsidian.css file.

2. In Obsidian click Settings->Plugins and turn on "Custom CSS".

3. Put the "obsidian.css" in your vault root folder.

4. Restart Obsidian.



## License

[MIT License](./LICENSE)